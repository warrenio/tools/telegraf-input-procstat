# Procstat Input Plugin for Containers

## Custom /proc Directory

This plugin is a fork of the official `procstat` input plugin of Telegraf,
transformed to `execd` type plugin.
It is augmented to find processes by looking at a `/proc` directory in some
custom location. This is useful for running Telegraf in a container while still
monitoring processes from the host.

For example, let's monitor `qemu` processes on the host and have their
`uuid=...` parameter as tag `uuid: ...`. This way we can easily monitor `qemu`
processes of a particular virtual machine.

`plugin.conf` for procstat:
```toml @plugin.conf
# Monitor process cpu and memory usage
[[inputs.procstat]]
  proc_dir = "$PROC_DIR"
  comm_pattern = "qemu"
  comm_param = "uuid"
```

`telegraf.conf` for telegraf:
```toml @telegraf.conf
command = ["/telegraf/procstat/procstat", "-poll_interval", "5s", "-config", "/telegraf/procstat/plugin.conf"]
```

Docker run command to make it happen, `telegraf-docker-image` could be custom built:
```shell
docker run -d --name telegraf \
  -v telegraf.conf:/etc/telegraf/telegraf.conf
  -v plugin.conf:/telegraf/procstat/plugin.conf
  -v procstat-binary:/telegraf/procstat/procstat
  -v /proc:/host/proc \
  -e PROCDIR=/host/proc \
  telegraf-docker-image:latest
```

# License

MIT License
